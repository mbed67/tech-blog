---
layout: page
title: About me
---

Hi, my name is Marieke Bednarczyk.

I became a software developer at the age of 47 after a whole life of doing other things in IT,
being a mother, and finding my purpose in life. I currently have 3.5 years of professional experience as a developer.

I try to strike a balance between being productive as a developer, following my curiosity and my drive to learn,
and returning to stillness, meditation, enjoying nature, slowing down, nurturing myself and others.

>“We do so much, we run so quickly, the situation is difficult, and many people say, "Don't just sit there, do something." But doing more things may make the situation worse. So you should say, "Don't just do something, sit there." Sit there, stop, be yourself first, and begin from there.”
>
>-- Thich Nhat Hanh --

I hope the posts in this blog will save you some precious time so you will have more left to relax and wind down.

For blogs about mindfulness and stuff I share about my spiritual path, look [here](https://theartofmindfulprogramming.com/blog/archive/)

