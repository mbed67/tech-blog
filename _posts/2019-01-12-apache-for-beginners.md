---
layout: post
show_meta: true
title: "Basic Apache configuration"
subtitle: "For PHP developers who want to go beyond copy-pasting"
---
Apache is one of the oldest and most popular web servers, and as a web developer you will most likely have to configure virtual hosts once in a while. If you are a bit like me, then the Apache configuration files can be quite overwhelming in the beginning. Therefore I would like to take you on a tour along the most common options that you will need to set up your webserver.

In this tour we will cover the following topics:
* [Looking around in the `/etc/apache2` directory](#looking)
* [Setting up Virtual Hosts for your websites](#setup)
* [Most common security measures](#security)
* [Enabling or disabling modules and sites](#enabling)
* [Logging](#logging)

### Context of this blog post
The directory structure and the name of the binary can differ depending on how Apache has been installed.
Sometimes you will see httpd and httpd-foreground, other times apache2 and apache2-foreground.
When you install apache2 using apt, you will get the latter. And the configuration will be found in `/etc/apache2`.

As a PHP developer and a docker user,
I most often deal with the configuration as can be found in the php docker images with the apache tags.
This is the apache2 installation you get from apt. So, even though I will not be discussing PHP in relationship to Apache,
we will be using the `php:7.3-apache` docker image.


If you have Docker installed, you can follow along with the examples.

## <a name="looking"></a>Looking around in the /etc/apache2 directory

First, let’s spin up a container with Apache installed, so that we can play around with the various options. We will log into the container and install vim as well. We will need this to edit the configuration files.
```
docker run --name apache -p 8000:80 -d php:7.3-apache
docker exec -it apache bash
apt-get update && apt-get install -y vim
```

Now that we are in the container, let’s have a look at the `/etc/apache2` directory. It contains the following files and folders:

* apache2.conf
* conf-available/
* conf-enabled/
* envvars
* magic
* mods-available/
* mods-enabled/
* ports.conf
* sites-available/
* sites-enabled/

### apache2.conf
It all starts with the file `apache2.conf`. This is the main configuration file that includes all other configuration files. It contains some default settings that apply to the entire system such as timeouts, log level, and some default security settings.

### ports.conf
This file contains the ports on which Apache listens.
The ports that are configured in this file, can be used in the section where you will configure your VirtualHost. But we will get to that later.

```
Listen 80

<IfModule ssl_module>
    Listen 443
</IfModule>

<IfModule mod_gnutls.c>
    Listen 443
</IfModule>
```

### envvars
This file sets some environment variables for apache2ctl (Apache HTTP server control interface). You will notice that a lot is configurable in Apache, such as the location of the log directory, or the name of the linux user used for running Apache. We can go along with the default values here.

### magic
This is a file that contains instructions for determining MIME type of a file. We will not discuss this here.

### available vs enabled
The directories in `/etc/apache2` come in pairs of available vs enabled. You can enable and disable modules, sites and configurations. When enabled, a symlink appears in the `enabled` directories to a corresponding file in the `available` directories.
More on this [later](#enabling).

## <a name="setup"></a>Setting up Virtual Hosts for your websites
Apache lets you run multiple websites on a single host. Let’s demonstrate how that works.

In the directory `sites-available` we can find two files:
* 000-default.conf
* default-ssl.conf

We will start with the first one, 000-default.conf. <br />
As you can see, all of the config files in Apache are very well documented. For now, we will however remove everything from this file.

We have already discovered that the `ports.conf` file contains the `Listen` directives that determine on which ports Apache will listen. In the `000-default.conf` file we will use those ports in the VirtualHost sections we are about to make for our websites. We are going to make two websites and for each one we are going to make a VirtualHost.

```
<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/www/mysite"
    ServerName www.mysite.com
</VirtualHost>
```

Both virtual hosts listen to port 80 on all interfaces (the asterisk * is a wildcard for all ip addresses). This means that the main server serves no requests. The ServerName directive makes sure that requests for `www.example.com` will be handled by the first virtual host and requests for `www.mysite.com` will be handled by the second one. The `DocumentRoot` directive points to the directory that holds the content of the site. If a request is received for another ServerName, let’s say `www.somethingelse.org` it will be served by the first virtual host as that is the default one.

Now that we have changed the configuration we will need to restart Apache. However, we have to be careful not to kill the apache process or we will stop the container and end up in our host OS again. We can restart apache on the fly like this:  `apache2ctl graceful`.

Okay, let’s make some content. We will create the directories `/www/example` and `/www/mysite`. <br />
From within the apache container run the following commands:

```
mkdir /www
cd /www
mkdir example
mkdir mysite
cd example
echo 'This is the example website' > index.html
cd ../mysite
echo 'This is the mysite website' > index.html
```

Next, add the following to your local `hosts` file (on MacOS and linux that will be `/etc/hosts`).
Just to be sure: this is NOT in the docker container, but on your host machine.

```
127.0.0.1  www.example.com www.mysite.com
```


Now we have a running webserver, we have two virtual hosts and we have content. Would we be able to see something already? Not quite yet. When we browse to `http://www.example.com:8000` we see the following:

![Forbidden](/img/forbidden.png){:class="img-responsive"}

#### DirectoryIndex
We get this error because we haven’t specified a default file to serve in the DocumentRoot. So let’s correct that.
We want to tell Apache to serve the file `index.html` from the `/www/example` directory. And there is more that we would like to configure on a directory level. So let’s start a new section within the virtual host: <Directory> and add the `DirectoryIndex` directive, to specify which file it can serve by default.

#### AllowOverride, Require all
Just adding the DirectoryIndex however, will still not let us see the website.
The default configuration from `/etc/apache2/apache2.conf` does not allow access to files and directories other than `/usr/share` and `/var/www` or a subdirectory of `/srv`. We, however, want to serve content from `/www/example` and `/www/mysite`. We can do that by adding the following directives:
* `AllowOverride None` : this in combination with `AllowOverrideList None` which is the default setting makes sure that within that directory `.htaccess` files will be ignored. So it will not be possible to override the settings with a `.htaccess` file.
* `Require all granted` : requests from all IP addresses are allowed access to this directory

```
<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com

    <Directory "/www/example">
        AllowOverride None
        Require all granted
        DirectoryIndex index.html
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/www/mysite"
    ServerName www.mysite.com

    <Directory "/www/mysite">
        AllowOverride None
        Require all granted
        DirectoryIndex index.html
    </Directory>
</VirtualHost>
```

After gracefully restarting Apache we should now be able to see our content on [http://www.example.com:8000](http://www.example.com:8000) and [http://www.mysite.com:8000](http://www.mysite.com:8000).

## <a name="security"></a>Most common security measures

### Turning ServerSignature off

Let’s browse to a non-existing page: [http://www.example.com:8000/non-existing](http://www.example.com:8000/non-existing])

![Notfound](/img/notfound.png){:class="img-responsive"}

As you can see the Apache version is shown as well as the operating system: Debian. This gives potential hackers too much information. Therefore we will hide it.

Add `ServerSignature Off` to the global section of your configuration file. Restart Apache and the information is no longer there.

Note that we don't change this in the file `conf-available/security.conf`. Changing it there would have the same effect, but it is convenient to have all the settings
in one file that you can keep in version control, so that you are free to switch to a newer docker image without having to copy over several files.

Because the `000-default.conf` is included after the files in the `conf-enabled` directory, the settings will override those of the `security.conf` file.

```
ServerSignature Off

<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com

    <Directory "/www/example">
        AllowOverride None
        Require all granted
        DirectoryIndex index.html
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/www/mysite"
    ServerName www.mysite.com

    <Directory "/www/mysite">
        AllowOverride None
        Require all granted
        DirectoryIndex index.html
    </Directory>
</VirtualHost>
```

### ServerTokens Prod

There is yet another place where too much information is shown, the Server response header.
Browsing to the non-existing page returns the following response header:

```
Server: Apache/2.4.25 (Debian)
```

Let’s hide this information as well. Add `ServerTokens Prod` to the global section of our configuration file.
Restart Apache and voila, gone!
It now only says:
```
Server: Apache
```


### Disable directory listing
In most cases you wouldn’t want anyone to be able to list the directories of your server. Our global configuration prevents that however and with our current VirtualHosts we aren’t overruling that. But let’s see which directive is responsible for this by trying it out.

(Btw. the global configuration allows directory listing for the `/var/www` directory, so always check twice how it works in your situation)

Change your config and add `Options Indexes`:

```
<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com

    <Directory "/www/example">
        AllowOverride None
        Require all granted
        DirectoryIndex index.html
        Options Indexes
    </Directory>
</VirtualHost>
```

Next restart Apache and create the following directory: `/www/example/.git`.
And then… have a look at the following url: [http://www.example.com:8000/.git/](http://www.example.com:8000/.git/)

Oops, we could have accidentally exposed our `.git` directory. Unless that directory would be above the DocumentRoot of course. But most of the time you wouln’t want anyone snooping around in your directories anyway.

So let’s change that to `Options -Indexes`, restart Apache and try again. Perfect, we are seeing the following message:

![Notfound](/img/forbidden-dir.png){:class="img-responsive"}

### Blocking all IP addresses except those you explicitly allow: whitelisting
Let’s take our VirtualHost again and then change `Require all granted` to `Require ip 172.17.0.1`

```
<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com

    <Directory "/www/example">
        AllowOverride None
        Require ip 172.17.0.1
        DirectoryIndex index.html
        Options -Indexes
    </Directory>
</VirtualHost>
```

Tip: from another terminal window run `docker logs apache` to see from which IP address the requests are coming.

Now after restarting Apache, browse to [http://www.example.com:8000](http://www.example.com:8000). We are still allowed to view the content.
Change the IP address in the configuration and we will be blocked.
We can use this to whitelist certain IP addresses and block all the rest.

### Don’t allow following symlinks
If your document root is located in the `/var/www` directory, then the global settings will allow the following of symlinks.
This can pose a security risk, because those symlinks can redirect you outside of the document root.

Let’s see how this works.
First of all, we’ll add `+FollowSymlinks` to our Options directive:

```
<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com

    <Directory "/www/example">
        AllowOverride None
        Require ip 172.17.0.1
        DirectoryIndex index.html
        Options -Indexes +FollowSymLinks
    </Directory>
</VirtualHost>
```

And then we’ll run the following commands:

```
cd /
echo 'This is a file outside of the document root' > test.html
cd /www/example
ln -s /test.html test.html
```

We have now created a symlink to a file in the root of the filesystem.
Now let’s restart Apache and browse to [http://www.example.com:8000/test.html](http://www.example.com:8000/test.html)

We are now seeing the content of a file outside of the document root. That might not be what you want, so let’s remove `+FollowSymLinks` and restart Apache.

## <a name="enabling"></a>Enabling or disabling modules and sites
We have seen that the subdirectories of `/etc/apache2` come in pairs of `available` and `enabled`.
The enabled subdirectories contains symlinks to corresponding files in the available subdirs.

Let's demonstrate how to enable or disable things in Apache.

### Modules
Apache is a modular system. Depending on what you need you have to install or enable more modules. If you list the directory `mods-available` you will see many more modules than you will see in the directory `mods-enabled`. The `rewrite` module, for instance, is available but not yet enabled, so if you need to do any redirects for your website, you will need to enable this module.
So, let's enable the `rewrite` module:

```
a2enmod rewrite
```

A symlink called `rewrite.load` has appeared in the `mods-enabled` directory. From now on we can use it's directives in our configuration files.

Let's disable it again:

```
a2dismod rewrite
```

And it's gone!

### Sites
For sites that are enabled, a symlink exists in the `sites-enabled` directory.
Let's remove the Virtual Host of www.mysite.com from `000-default.conf` and put it in it's own file: `sites-available/mysite.conf` and restart Apache.

When we now browse to [http://www.mysite.com:8000](http://www.mysite.com:8000) we see the content of the `example` website. How is that possible?

Well, our mysite is not yet enabled. And our default site, the site that catches everything that cannot be mapped to a specific Virtual Host, is the `example` site.

So, let's enable mysite:

```
a2ensite mysite
```

As you can see, a symlink has appeared in the `sites-enabled` directory.
Let's restart apache and check [http://www.mysite.com:8000](http://www.mysite.com:8000) again.

We will now see the content of mysite.

We can disable this site again with the following command:

```
a2dissite mysite
```

## <a name="logging"></a>Logging

In the file `/etc/apache2/envvars` we can see that the default directory where we can find our log files is `/var/log/apache2`.
There are two kinds of log files we can expect in Apache:
* error logfiles: any errors are logged here
* access logfiles: all requests are logged here

When you don't specify those log files in your Virtual Hosts, then you will have three log files in your log directory:

* error.log
* access.log
* other_vhosts_access.log

But, as we are dealing with a docker container, these files have been redirected to stdout / stderr as you can see in this [Dockerfile](https://github.com/docker-library/php/blob/a280ab8e8790052338ce59a1fee739df8f831f16/7.3/stretch/apache/Dockerfile#L76).
That is more convenient, because we will most likely use the command `docker logs` to inspect the log files, rather than reading actual files from within the container.

For demonstration purposes however, let's define some log files for our example website.

```
<VirtualHost *:80>
    DocumentRoot "/www/example"
    ServerName www.example.com

    ErrorLog ${APACHE_LOG_DIR}/example_error.log
    CustomLog ${APACHE_LOG_DIR}/example_access.log combined

    <Directory "/www/example">
        AllowOverride None
        Require ip 172.17.0.1
        DirectoryIndex index.html
        Options -Indexes +FollowSymLinks
    </Directory>
</VirtualHost>
```

And restart Apache.

When we inspect the `/var/log/apache2` directory, we will see the two new log files.
Most Apache processes run under the `www-data` user so let's make that the owner of the log files:

```
chown www-data:www-data /var/log/apache2/example*
```

Now when we browse to [http://www.example.com:8000](http://www.example.com:8000) a log record will be created in `/var/log/apache2/example-access.log`.
And when an error happens, we will see that in `/var/log/apache2/example-error.log`.

You might wonder what `combined` means in the `CustomLog directive`.
In the file `/etc/apache2/apache2.conf` several LogFormats are defined. Each of them has a name that you can refer to when using the CustomLog directive.

For more information about the options used in the LogFormat see [this page](https://httpd.apache.org/docs/2.4/mod/mod_log_config.html#formats)


## Conclusion
The Apache webserver is highly configurable.
This blog post covered the most basic options you need to understand to get your webserver up-and-running with a php:7.3-apache docker container.
More detailed information can be found on the [Apache Documentation page](https://httpd.apache.org/docs/2.4/)
